package io.github.joxit.osm.service;

import io.github.joxit.osm.model.Tile;
import io.github.joxit.osm.utils.Svg;
import mil.nga.sf.geojson.GeoJsonObject;
import org.springframework.stereotype.Service;

import java.io.*;

/**
 * Service pour retourner les tuiles.
 *
 * @author Jones Magloire @Joxit
 * @since 2019-11-03
 */
@Service
public class TileService {

  /**
   * Ici il faut prendre les coordonnées de la tuile et renvoyer la donnée PNG associée.
   * Vous pouvez y ajouter des fonctionnalités en plus pour améliorer les perfs.
   *
   * @param tile qu'il faut renvoyer
   *
   * @return le byte array au format png
   */
  public byte[] getTile(Tile tile) {

    if(tile.getZ() < 0){
      throw new IllegalArgumentException("error 400");
    }
    if(tile.getX() < 0){
      throw new IllegalArgumentException("error 400");
    }
    if(tile.getY() < 0){
      throw new IllegalArgumentException("error 400");
    }
    if (tile.getZ() > 24 || tile.getX() > Math.pow(2,tile.getZ()) || tile.getY() > Math.pow(2, tile.getZ())) {
      throw new IllegalArgumentException("Zoom trop élevé");
    }
    return Svg.getTile(tile);
  }

  /**
   * @return le contenu du fichier prefectures.geojson
   */

  public String getPrefectures() {
    String fichie = "osm-core\\src\\main\\resources\\prefectures.geojson";
    StringBuilder chaine= new StringBuilder();

    //lecture du fichier texte
    try{
      InputStream ips=new FileInputStream(fichie);
      InputStreamReader ipsr=new InputStreamReader(ips);
      BufferedReader br=new BufferedReader(ipsr);
      String ligne;
      while ((ligne=br.readLine())!=null){
        chaine.append(ligne).append(" ");
      }
      br.close();
    }
    catch (Exception e){
      System.out.println(e.toString());
    }
      return chaine.toString();
  }

  /**
   * Il faudra créer votre DAO pour récuperer les données.
   * Utilisez ce que vous voulez pour faire le DAO.
   *
   * @return les éléments contenus dans la base de données
   */
  public GeoJsonObject getPOIs() {
    return null;
  }


}
